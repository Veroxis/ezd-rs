{
  description = "build env for rust projects";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { self, flake-utils, nixpkgs, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rustVersion = "latest";
        rust = pkgs.rust-bin.stable.${rustVersion}.default.override {
          extensions = [ "rust-src" ];
          targets = [
            "x86_64-unknown-linux-musl"
            "aarch64-unknown-linux-musl"
            "arm-unknown-linux-musleabihf"
          ];
        };
        cargoBuildInputs = with pkgs; lib.optionals stdenv.isDarwin [
          darwin.apple_sdk.frameworks.CoreServices
        ];
      in
      {
        devShell = pkgs.mkShell {
          ZIG_GLOBAL_CACHE_DIR = "./target/zig_cache/global";
          ZIG_LOCAL_CACHE_DIR = "./target/zig_cache/local";
          RUST_BACKTRACE = 1;
          RUST_LOG = "info";

          buildInputs = ([
            rust
            cargoBuildInputs
          ]) ++ (with pkgs; [
            cargo-edit
            cargo-nextest
            cargo-zigbuild
            pkg-config
            zig
            just
          ]);

          shellHook = ''
            alias ll="ls -l"
          '';
        };
      }
    );
}
