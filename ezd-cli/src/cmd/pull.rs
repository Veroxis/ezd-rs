use std::collections::HashMap;
use std::fs::read_to_string;

use colored::Colorize;
use ezd_core::config::ezd::{find_config_path, Ezd, EzdError, FindConfigPathError};
use ezd_core::shell::{execute, ShellError};
use thiserror::Error;

#[derive(Debug, Error)]
pub enum PullError {
    #[error("Io: `{0}`")]
    Io(#[from] std::io::Error),

    #[error("FindConfigPath: `{0}`")]
    FindConfigPath(#[from] FindConfigPathError),

    #[error("Shell: `{0}`")]
    Shell(#[from] ShellError),

    #[error("SerdeJson: `{0}`")]
    SerdeJson(#[from] serde_json::Error),

    #[error("Ezd: `{0}`")]
    Ezd(#[from] EzdError),
}

pub fn pull() -> Result<(), PullError> {
    let config_path = find_config_path()?;
    let config = read_to_string(config_path)?;
    let ezd = Ezd::try_from(config)?;
    let hashmap = HashMap::new();
    for (_, task) in ezd.tasks.docker_seq.iter() {
        let image = task.config.image.as_str();
        let cmd = format!("docker pull {image}");
        println!(
            "[{}:{}] {} {}",
            "ezd".green(),
            "pull".green(),
            ">_".blue(),
            cmd.purple()
        );
        execute(cmd.as_str(), &hashmap)?;
    }
    Ok(())
}
